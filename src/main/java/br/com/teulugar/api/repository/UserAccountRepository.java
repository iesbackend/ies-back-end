package br.com.teulugar.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.teulugar.api.entity.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long>{
	UserAccount findByUsername(String username);
}
