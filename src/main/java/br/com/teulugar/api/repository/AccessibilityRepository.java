package br.com.teulugar.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.teulugar.api.entity.Accessibility;

public interface AccessibilityRepository extends JpaRepository<Accessibility, Integer>{
	Accessibility findByType(String type);
}
