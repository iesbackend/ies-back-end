package br.com.teulugar.api.repository;

import br.com.teulugar.api.entity.CompanyAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyAccountRepository extends JpaRepository<CompanyAccount, Long> {
}
