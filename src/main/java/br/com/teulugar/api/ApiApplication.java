package br.com.teulugar.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.teulugar.api.entity.Accessibility;
import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.repository.AccessibilityRepository;
import br.com.teulugar.api.repository.UserAccountRepository;
import br.com.teulugar.api.services.UserAccountService;

@SpringBootApplication
public class ApiApplication implements CommandLineRunner {
	
	@Autowired 
	private AccessibilityRepository accessibilityRepository;
	
	@Autowired
	private UserAccountRepository userRepository;
	
	@Autowired
	private UserAccountService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
	
		if(!userRepository.findAll().isEmpty()) {
			return;
		}
		
		UserAccount admin = new UserAccount();
		admin.setAdmin(true);
		admin.setUsername("admin");
		admin.setPassword("admin1234");
		
		userService.save(admin);
		
		UserAccount estab = new UserAccount();
		estab.setPassword("12345678");
		estab.setUsername("restaurante");
		
		userService.save(estab);		
		
		if(!accessibilityRepository.findAll().isEmpty()) {
			return;
		}
		
		Accessibility visual = new Accessibility();
		Accessibility fisica = new Accessibility();
		Accessibility auditiva = new Accessibility();
		
		visual.setType("Visual");
		fisica.setType("Física");
		auditiva.setType("Auditiva");
		
		accessibilityRepository.save(auditiva);
		accessibilityRepository.save(fisica);
		accessibilityRepository.save(visual);
	}

}
