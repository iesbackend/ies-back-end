package br.com.teulugar.api.exception;

public class UserAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 4213847459987342803L;
	
	public UserAlreadyExistsException() {
		super();
	}
	
	public UserAlreadyExistsException(final String msg) {
		super(msg);
	}
	
}
