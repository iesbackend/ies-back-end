package br.com.teulugar.api.exception;

public class PasswordInvalidException extends RuntimeException {

	private static final long serialVersionUID = -4682096407174946217L;
	
	public PasswordInvalidException() {
		super();
	}
	
	public PasswordInvalidException(final String msg) {
		super(msg);
	}
}
