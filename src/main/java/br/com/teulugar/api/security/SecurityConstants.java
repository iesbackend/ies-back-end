package br.com.teulugar.api.security;

public enum SecurityConstants {
	SECRET("#@$%K3v3K%$#@"),
	TOKEN_PREFIX("Bearer "),
	HEADER("Authorization"),
	EXPIRATION("86400000"),
	SIGNIN("/user");
	
	private String constant;
	
	private SecurityConstants(String constant) {
		this.constant = constant;
	}
	
	public String getConstant() {
		return this.constant;
	}
}
