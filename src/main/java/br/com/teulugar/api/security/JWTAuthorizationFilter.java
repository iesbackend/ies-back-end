package br.com.teulugar.api.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import br.com.teulugar.api.services.CustomUserDetailService;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	
	private final CustomUserDetailService customUserDetail;
	
	public JWTAuthorizationFilter(CustomUserDetailService customUserDetail, AuthenticationManager authenticationManager) {
		super(authenticationManager);
		this.customUserDetail = customUserDetail;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		String header = request.getHeader(SecurityConstants.HEADER.getConstant());
		
		if(header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX.getConstant())) {
			chain.doFilter(request, response);
			return;
		}
		
		UsernamePasswordAuthenticationToken userPassAuthToken = this.getAuthenticationToken(request);
		
		SecurityContextHolder.getContext().setAuthentication(userPassAuthToken);
		chain.doFilter(request, response);
	}
	
	private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.HEADER.getConstant());
		
		if(token == null) return null;
		
		String username = Jwts.parser().setSigningKey(SecurityConstants.SECRET.getConstant())
				.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX.getConstant(), ""))
				.getBody().getSubject();
		
		UserDetails userDetails = customUserDetail.loadUserByUsername(username);
		
		return username != null ? new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities() ) : null;
	}
}
