package br.com.teulugar.api.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.teulugar.api.entity.UserAccount;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	private AuthenticationManager authManager;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authManager = authenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		try {
			UserAccount user = new ObjectMapper().readValue(request.getInputStream(), UserAccount.class);
			
			return this.authManager.authenticate(
					new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
			
		} catch(IOException ioe) {
			ioe.printStackTrace();
			throw new RuntimeException(ioe);
		}
		
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		String token = Jwts.builder()
				.setSubject( ((User) authResult.getPrincipal()).getUsername() )
				.setExpiration( new Date(System.currentTimeMillis() + Long.valueOf(SecurityConstants.EXPIRATION.getConstant())) )
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET.getConstant())
				.compact();
		
		String bearerToken = SecurityConstants.TOKEN_PREFIX.getConstant() + token;
		
		response.getWriter().write(bearerToken);
		
		response.addHeader(SecurityConstants.HEADER.getConstant(), bearerToken);
		
	}
}
