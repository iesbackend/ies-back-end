package br.com.teulugar.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.exception.PasswordInvalidException;
import br.com.teulugar.api.exception.UserAlreadyExistsException;
import br.com.teulugar.api.repository.UserAccountRepository;

@Service
public class UserAccountService {
	
	@Autowired
	private UserAccountRepository userRepository;
	
	private static final BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
	
	@Transactional(rollbackFor = Exception.class)
	public UserAccount save(UserAccount user) throws UserAlreadyExistsException, 
													 PasswordInvalidException, 
													 Exception {
		
		try {
			this.validPassword(user.getPassword());
			this.validUser(user.getUsername());

			user.setAdmin(false);
			
			user.setPassword(bCrypt.encode(user.getPassword()));
				
			return userRepository.save(user);
			
		} catch(UserAlreadyExistsException userException) {
			throw userException;
			
		} catch(PasswordInvalidException passException) {
			throw passException;
			
		} catch(Exception e) {
			throw e;
		}
		
	}
	
	@Transactional(rollbackFor = Exception.class)
	public UserAccount saveAdmin(UserAccount user) throws UserAlreadyExistsException, 
													 PasswordInvalidException, 
													 Exception {
		
		try {
			this.validPassword(user.getPassword());
			this.validUser(user.getUsername());

			user.setAdmin(true);
			
			user.setPassword(bCrypt.encode(user.getPassword()));
				
			return userRepository.save(user);
			
		} catch(UserAlreadyExistsException userException) {
			throw userException;
			
		} catch(PasswordInvalidException passException) {
			throw passException;
			
		} catch(Exception e) {
			throw e;
		}
		
	}
	
	private boolean validUser(String username) {
		if(userRepository.findByUsername(username) != null) {
			throw new UserAlreadyExistsException("User already registred");
		} else {
			return true;
		}
		
		
	}
	
	private boolean validPassword(String password) {
		if(password.isEmpty() || password.length() < 8) {
			throw new PasswordInvalidException("Invalid password");
		} else {
			return true;
		}
	}
	
	public UserAccount getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
