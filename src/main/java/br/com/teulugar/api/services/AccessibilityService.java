package br.com.teulugar.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.teulugar.api.entity.Accessibility;
import br.com.teulugar.api.repository.AccessibilityRepository;

@Service
public class AccessibilityService {
	
	@Autowired
	private AccessibilityRepository accessRepository;
	
	@Transactional(rollbackFor=Exception.class)
	public Accessibility save(Accessibility access) throws Exception {
		return this.accessRepository.save(access);
		
	}
	
	public List<Accessibility> getAll() {
		return accessRepository.findAll();
	}
	
	public Accessibility getByType(String type) throws Exception {
		Accessibility access = this.accessRepository.findByType(type);
		
		if(access != null) {
			return access;
			
		} else {
			throw new Exception();
		}
	}
}
