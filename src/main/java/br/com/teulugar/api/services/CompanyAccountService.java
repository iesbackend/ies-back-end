package br.com.teulugar.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.teulugar.api.entity.CompanyAccount;
import br.com.teulugar.api.repository.AccessibilityRepository;
import br.com.teulugar.api.repository.CompanyAccountRepository;

@Service
public class CompanyAccountService {

    @Autowired
    private CompanyAccountRepository companyRepository;
    
    @Autowired
    private AccessibilityRepository accesRepository;

    @Transactional(rollbackFor = Exception.class)
    public CompanyAccount save(CompanyAccount company) throws Exception {
        try {
        	company.setAccessibilities(accesRepository.findAll());
            return companyRepository.save(company);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<CompanyAccount> getAll() {
    	return companyRepository.findAll();
    }
    
    public CompanyAccount getById(long id) throws Exception {
    	if(companyRepository.existsById(id)) {
    		return companyRepository.findById(id).get();
    		
    	} else {
    		throw new Exception("Company not found!");
    	}
    }

}
