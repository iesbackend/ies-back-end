package br.com.teulugar.api.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.repository.UserAccountRepository;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailService implements UserDetailsService {
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAccount userAccount = Optional.ofNullable(userAccountRepository.findByUsername(username))
				.orElseThrow(() -> new UsernameNotFoundException("User not found!"));
	
		
		return new User(userAccount.getUsername(), userAccount.getPassword(), this.getAuthority(userAccount));
	}
	
	private List<GrantedAuthority> getAuthority(UserAccount userAccount) {
		
		if(userAccount.isAdmin()) {
			return AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN");
		} else {
			return AuthorityUtils.createAuthorityList("ROLE_USER");
		}
	}
}
