package br.com.teulugar.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.teulugar.api.dto.ErrorDTO;
import br.com.teulugar.api.entity.CompanyAccount;
import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.services.CompanyAccountService;
import br.com.teulugar.api.services.UserAccountService;

@RestController
@RequestMapping(path="/company")
public class CompanyAccountController {


    @Autowired
    private CompanyAccountService companyAccountService;
    
    @Autowired
    private UserAccountService userService;


    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAll() {
        List<CompanyAccount> list = companyAccountService.getAll();
        
        if(!list.isEmpty()) {
        	return new ResponseEntity<List<CompanyAccount>>(list, HttpStatus.OK);
        } else {
        	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> save(@Valid @RequestBody CompanyAccount company, Authentication auth) {
        try {
        	UserAccount user = userService.getUserByUsername(auth.getName());
        	company.setUser(user);
        	
            return new ResponseEntity<CompanyAccount>(companyAccountService.save(company), HttpStatus.CREATED);
            
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorDTO("COMPANY_ERROR", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") long id) {
    	try {
    		return new ResponseEntity<CompanyAccount>(companyAccountService.getById(id), HttpStatus.OK);
    		
    	} catch(Exception e) {
    		ErrorDTO error = new ErrorDTO("COMPANY_ERROR", e.getMessage());
    		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    	}
    }
}
