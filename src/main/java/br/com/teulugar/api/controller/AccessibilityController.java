package br.com.teulugar.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.teulugar.api.dto.ErrorDTO;
import br.com.teulugar.api.entity.Accessibility;
import br.com.teulugar.api.services.AccessibilityService;

@RestController
@RequestMapping(path="/accessibility")
public class AccessibilityController {
	
	@Autowired
	private AccessibilityService accessService;
	
	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> save(@Valid @RequestBody Accessibility access) {
		try {
			return new ResponseEntity<>(accessService.save(access), HttpStatus.CREATED);
			
		} catch(Exception ex) {
			ErrorDTO error = new ErrorDTO("ACCESSIBILITY_ERROR", ex.getMessage());
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping
	public ResponseEntity<List<Accessibility>> getAll() {
		List<Accessibility> list = accessService.getAll();
		
		if(!list.isEmpty()) {
			return new ResponseEntity<List<Accessibility>>(list, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping(value="/{type}")
	public ResponseEntity<?> getByType(@PathVariable("type") String type) {
		try {
			return new ResponseEntity<>(this.accessService.getByType(type), HttpStatus.OK);
			
		} catch(Exception ex) {
			ErrorDTO error = new ErrorDTO("ACCESSIBILITY_ERROR", ex.getMessage());
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}
	}
}
