package br.com.teulugar.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.teulugar.api.dto.ErrorDTO;
import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.exception.PasswordInvalidException;
import br.com.teulugar.api.exception.UserAlreadyExistsException;
import br.com.teulugar.api.services.UserAccountService;

@RestController
@RequestMapping(path="/user")
public class UserAccountController {
	
	@Autowired
	private UserAccountService userService;
	
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody UserAccount user) {
		try {
			return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
			
		} catch(UserAlreadyExistsException ex) {
			return new ResponseEntity<>(new ErrorDTO("USER_ERROR", ex.getMessage()), HttpStatus.BAD_REQUEST);
			
		} catch(PasswordInvalidException ex) {
			return new ResponseEntity<>(new ErrorDTO("USER_ERROR", ex.getMessage()), HttpStatus.BAD_REQUEST);
			
		} catch(Exception e) {
			return new ResponseEntity<>(new ErrorDTO("USER_ERROR", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
}
