package br.com.teulugar.api;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.teulugar.api.entity.UserAccount;
import br.com.teulugar.api.services.UserAccountService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserAccountRepositoryTest  {
	
	@Autowired
	private UserAccountService userService;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void createUserAccountEntity() {
		String username = "fulano@fulano.com.br";
		String password = "s3nh4d3ful4n0";
		
		UserAccount user = new UserAccount();
		user.setUsername(username);
		user.setPassword(password);
		
		try {
			this.userService.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assertions.assertThat(user.getId()).isNotNull();
		Assertions.assertThat(user.getUsername()).isEqualTo(username);
		Assertions.assertThat(user.getPassword()).isEqualTo(password);
		
	}
}
